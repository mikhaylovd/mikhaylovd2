import java.util.Scanner;
public class Main {
    static int getInt(){
        Scanner scanner = new Scanner(System.in);
        while (true){
            if (scanner.hasNextInt()) return scanner.nextInt();
            System.out.println("Неверный формат, попробуйте еще раз");
            scanner.next();
        }
    }
    static boolean getBoolean(){
        Scanner scanner = new Scanner(System.in);
        while (true){
            if (scanner.hasNextBoolean()) return scanner.nextBoolean();
            System.out.println("Неверный формат, попробуйте еще раз");
            scanner.next();
        }
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int temperature, windSpeed;
        boolean rain;
        System.out.println("Введите температуру");
        temperature = getInt();
        System.out.println("Введите скорость ветра (м/c)");
        windSpeed = getInt();
        System.out.println("Введите, есть ли на улице дождь (true/false)");
        rain = getBoolean();
        if (rain) System.out.println("На улице дождь, лучше останьтесь дома!");
        else if (temperature >= 30 && temperature <=35 && windSpeed>= 3 && windSpeed <=8) System.out.println("На улице жарковато, но небольшой ветер вас спасет! Можно идти гулять");
        else if (temperature > 30) System.out.println("На улице слишком жарко, оставайтесь дома!");
        else if (windSpeed > 8) System.out.println("Лучше оставайтесь дома, на улице очень сильный ветер");
        else if (temperature < 15) System.out.println("На улице прохладно, оставайтесь дома!");
        else System.out.println("На улице отличная погода, можете смело идти гулять!");
    }
}
